﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateEntryApplication
{
    public class ResultDTO
    {
        public string Result { get; set; }
        public Boolean isSuccess { get; set; }
    }
}
