﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GateEntryApplication
{
    public class FileUpload
    {
        
        public ResultDTO AddImages(string path,string empName,IFormFile file)
        {
            var res = new ResultDTO();
            try {
                var PathWithFolderName = System.IO.Path.Combine(path + "/assets/images", empName);
                if (!Directory.Exists(PathWithFolderName))
                {
                    DirectoryInfo di = Directory.CreateDirectory(PathWithFolderName);
                    using (FileStream fs = System.IO.File.Create(di + "/" + file.FileName))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                }
                else
                {
                    using (FileStream fs = System.IO.File.Create(PathWithFolderName + "/" + file.FileName))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                }
                res.isSuccess = true;
                res.Result = "Image save successfully";
            }
            catch (Exception ex)
            {
                res.isSuccess = false;
                res.Result ="Error-----"+ ex.Message+ "-----Error";
            }
            
            return res;
        }
        

    }
}
